# Work in Progress. See \#Planning
# VehicleSim
VehicleSim for ICS4U. Starter code from Mr. Jordan Cohen.

## Purpose
Controlled randomize generation of vehicles and pedestrians with funny effects.

## Requirements
Dependencies: Greenfoot, OpenJDK(>= 17)
> You people should use a package manager and stop downloading from websites. It saves time and protects you phishing sites, plus it is really not that hard.
### Windows
```winget install GreenfootTeam.Greenfoot AdoptOpenJDK.OpenJDK.17```
### macOS
```brew install --cask openjdk@17 greenfoot```
### Linux
#### Arch Linux (btw) based (with AUR)
```yay -S jdk17-openjdk java17-openjfx-bin greenfoot```
#### Other distros: Flatpak (newest update seems broken)
```flatpak install org.greenfoot.Greenfoot```

## Download
### Using git (recommend)
```git clone https://github.com/Adrian400811/VehicleSim```  

Update  
```git pull```

### Download as zip
Press code -> zip -> download


## Run
Open directory in Greenfoot, press Run

## Planning
See [Progress Tracker](https://github.com/Adrian400811/VehicleSim/issues/1)

TL;DR: Car crashes, tow trucks, people screaming, better UI, allow customizing.

## Authors
Original: Mr. Jordan Cohen

Modified: Adrian400811

## Credits
Vehicle Sprites: [Awesome Car Pack - UnLucky Studio (Sujit Yadav)](https://unluckystudio.com/game-art-giveaway-7-top-down-vehicles-sprites-pack/)

Explosion Image: [Explosion - pixelartmaker.com](https://pixelartmaker.com/art/695c3a296d3fc8c)

Explosion Sound: [Small Bomb Explosion Sound Effect](https://youtu.be/9FMquJzgDGQ)

Scream: Vilhelm Scream (Internet Meme, Source Unknown)

Truck Sound: [Truck In Reverse - Beeping - Sound Effect](https://youtu.be/fRzYqsDSplg)
